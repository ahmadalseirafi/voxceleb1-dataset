# Dataset Filtering And Tagging (Voxceleb1) Code Usage


# NoteBook 

The code to handle the dataset is a jupyter notebook .

the notebook contains very clear comments about each cell so you will not have a problem understanding anything.

it is divided to clear sections where each section is responsible of some specific task like **downloading data section** , **preprocessing section** , **transcription section** .. etc.  .

other than the notebook comments there are some things that need to be done ,and I will mention them in the below sections

# Download Dataset

to start working on the data you must download it first .

the data is available as a zip file and you can download it using this command.

    !wget www.robots.ox.ac.uk/~vgg/data/voxceleb/data/vox1_test_txt.zip

we must unzip the data after downloading it using this command

    !unzip -q vox1_test_txt.zip

now the data is ready for our code to work on it and download the audio from YouTube.

# Packages

in order to use the code you must have some packages installed so please execute the following commands for packages installation:

    !pip install pytube pydub
    !pip install SpeechRecognition
    !pip install -U openai-whisper
    
    // this package was used to reduce audio noise 
    !pip install noisereduce

also ffmpeg must be installed

    !apt install ffmpeg

# Resulting Data

the code gives us the option to save the data as a csv file after preprocessing and labeling for future usages.

