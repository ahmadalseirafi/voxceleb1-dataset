# Dataset Filtering And Tagging (Voxceleb1)

In this file I will be explaining the the thought process behind what Id did and which tools I used to complete the task.  

# Dataset
![](https://production-media.paperswithcode.com/datasets/Screen_Shot_2021-01-08_at_4.15.45_PM.png)

The **VoxCeleb1** dataset is a collection of audio-visual recordings of speakers collected from YouTube. It's primarily used for speaker identification and verification tasks, **VoxCeleb1** contains over 100,000 utterances for 1,251 celebrities, extracted from videos uploaded to YouTube.

in the challenge details I was told that I can only work on the test set , which I did.
test set includes 40 speaker and 677 videos , so the test set of VoxCeleb1 typically contains around 4874 utterances from various speakers.

due to resources considerations I was unable to download the whole test set so I picked 6 speakers only form the Test set  , so I had  508 utterances  to work with. I think this is not going to be a problem because it is a proof of concept , and I am sure with more resources the full dataset won't be a problem for me .


## Dataset Download

The dataset audios and videos were not available on the website provided in the challenge . it was available like txt files .
each text file contained a range of video frames describing the utterance , so I had to come up with a way to download the full dataset by iterating through each speaker folder and then for each YouTube video  id Folder for that speaker.
inside each YouTube video folder we have .txt files that represent each .wav file with a range of frames and more information so I downloaded the video and then trimmed it based on the frames  range  converted it to .wav audio.

and now I have the dataset in each original form like this .

    VoxCeleb1/
    │
    ├── id10001/
    │   ├── 00001.wav
    │   ├── 00002.wav
    │   └── ...
    │
    ├── id10002/
    │   ├── 00001.wav
    │   └── ...
    │
    └── ...


## Labeling (Transcription)

I decided to apply **Transcription**. 

Transcription in AI refers to the process of converting spoken language into written text using artificial intelligence technologies.

![](https://www.corporatecomplianceinsights.com/wp-content/uploads/2019/05/speech-to-text.jpg)

## Audio Preprocessing

it is very important to preprocess data before feeding it to a model .

when it comes to Audio two preprocessing methods come to my mind :

### 1. **Removing Short Audio Clips**

Audio clips shorter than a certain duration (e.g., 2 seconds) may not contain sufficient information for reliable analysis

### 2. **Noise Cancellation**

Noise cancellation helps improve the quality of the audio by reducing background noise. This can be done using various techniques, including spectral gating, adaptive filtering, or using machine learning models.

I applied both actually ,but I notices that noise cancellation made the sound lower , which had a very bad impact on the labeling process.
so in the end I only applied getting rid of short audios ,because they had no meaning.

## Transcription Model : OpenAI  Whisper

![](https://assets-global.website-files.com/614c82ed388d53640613982e/63eb5ebedd3a9a738e22a03f_open%20ai%20whisper.jpg)

**Whisper** is an automatic speech recognition (ASR) system trained on 680,000 hours of multilingual and multitask supervised data collected from the web.
Whisper from OpenAI has been gaining attention with its impressive **95% to 98.5%** accuracy rate (without manual intervention).

due to its huge capabilities and high accuracy I decided to use it for labeling  the audio files.

## More Labeling (Sentiment Analysis)

![](https://www.voxco.com/wp-content/uploads/2021/09/Sentiment-Analysis1.jpg)

now after getting the text from the audio files , it is a good time to apply **sentiment analysis** on the resulting text .

I actually tried to do so after I finished transcription , but for time reasons I couldn't apply it , because I didn't want to use an arbitrary model that is not efficient or robust enough .

## Labelling Result

after the transcription is done I save the resulting dataset  as a csv file like this : 

| Person | Video      | Utterance | Text                                                |
|--------|------------|-----------|-----------------------------------------------------|
| id10275   | Mdk1SXywHck  | 00011.wav         | I mean, the reason I like the show is that it has.            | |


to make it easy for future usage or to train with .


## Download Labelling Result & Meta Data

  

both  the resulting labeled data set can be download [here](https://drive.google.com/file/d/1AasEMf8CNNV__ZTp76MpWXgCFdKKTu53/view?usp=drive_link).

and the metadata [here](https://drive.google.com/file/d/14M46tO9I1FeF0CThPa-q_yua_gHlTTue/view?usp=sharing).

 
